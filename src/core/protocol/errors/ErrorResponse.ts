/*
 * Copyright (C) 2018-2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {ValidationError} from 'jsonschema';

/**
 * A generic error that can be returned by the backend if somethings fails during the processing of a request
 *
 * @validatable
 */
export interface SCErrorResponse extends Error {
  /**
   * Additional data that describes the error
   */
  additionalData?: any;

  /**
   * HTTP status code to return this error with
   */
  statusCode: number;
}

/**
 * An error that can be created by the backend during the processing of a request
 */
export abstract class SCError implements SCErrorResponse {
  /**
   * Call stack of the error
   */
  stack?: string;

  /**
   * Instatiate an SCError
   *
   * @param name Name of the error
   * @param message Message of the error
   * @param statusCode  HTTP status code to return this error with
   * @param stack Set to true if a stack trace should be created
   */
  constructor(public name: string, public message: string, public statusCode: number, stack?: boolean) {
    // generate stacktrace if needed
    if (stack) {
      this.stack = (new Error()).stack;
    }
  }
}

/**
 * An error that is returned when the validation of a request fails
 */
export class SCValidationErrorResponse extends SCError {
  /**
   * List of validatation errors
   */
  additionalData: ValidationError[];

  /**
   * Create a SCValidationErrorResponse
   *
   * @param errors List of validation errors
   * @param stack Set to true if a stack trace should be created
   */
  constructor(errors: ValidationError[], stack?: boolean) {
    super('ValidationError', 'Validation of request failed', 400, stack);
    this.additionalData = errors;
  }
}

/**
 * An error that is returned when the content type of the request is not supported
 */
export class SCUnsupportedMediaTypeErrorResponse extends SCError {
  /**
   * Create a SCUnsupportedMediaTypeErrorResponse
   *
   * @param stack Set to true if a stack trace should be created
   */
  constructor(stack?: boolean) {
    super('UnsupportedMediaTypeError', 'Unsupported media type', 415, stack);
  }
}

/**
 * An error that is returned, when the used HTTP method is not allowed on the requested route
 */
export class SCMethodNotAllowedErrorResponse extends SCError {
  /**
   * Create a SCMethodNotAllowedErrorResponse
   *
   * @param stack Set to true if a stack trace should be created
   */
  constructor(stack?: boolean) {
    super('MethodNotAllowedError', 'HTTP method is not allowed on this route', 405, stack);
  }
}

/**
 * An error that is returned, when the request body is too large.
 */
export class SCRequestBodyTooLargeErrorResponse extends SCError {
  /**
   * Create a SCRequestBodyTooLargeErrorResponse
   * @param stack Set to true if a stack trace should be created
   */
  constructor(stack?: boolean) {
    super('RequestBodyTooLargeError', 'The request body is too large.', 413, stack);
  }
}

/**
 * An error that is returned, when to many request are submitted at once
 */
export class SCTooManyRequestsErrorResponse extends SCError {
  /**
   * Create a SCTooManyRequestsErrorResponse
   *
   * @param stack Set to true if a stack trace should be created
   */
  constructor(stack?: boolean) {
    super('TooManyRequestsError', 'Too many requests. You can not submit more than 5 queries an once', 429, stack);
  }
}

/**
 * An error that is returned when the requested route or resource was not found
 */
export class SCNotFoundErrorResponse extends SCError {
  /**
   * Create a SCNotFoundErrorResponse
   *
   * @param stack Set to true if a stack trace should be created
   */
  constructor(stack?: boolean) {
    super('NotFoundError', 'Resource not found', 404, stack);
  }
}

/**
 * An error that is returned when the request is in the right format, but contains parameters that are invalid or not
 * acceptable.
 */
export class SCParametersNotAcceptable extends SCError {
  /**
   * Create a ParametersNotAcceptable
   *
   * @param message contains more details to what you did wrong
   * @param stack Set to true if a stack trace should be created
   */
  constructor(message: string, stack?: boolean) {
    super('ParametersNotAcceptable', message, 406, stack);
  }
}

/**
 * An error that is returned when a plugin with the same name is already registered, to prevent two copies of a plugin
 * running at the same time.
 * This usually indicates that there is more than one instance a plugin running.
 */
export class SCPluginAlreadyRegisteredErrorResponse extends SCError {
  /**
   * Create a PluginAlreadyRegisteredError
   *
   * @param message contains potential differences in other parameters outside of the name
   * @param stack Set to true if a stack trace should be created
   */
  constructor(message: string, stack?: boolean) {
    super('PluginRegisteringFailedError', message, 409, stack);
  }
}

/**
 * An error that is returned whenever there is an unexpected error while creating a plugin
 */
export class SCPluginRegisteringFailedErrorResponse extends SCError {
  /**
   * Create a PluginRegisteringFailedError
   *
   * @param message Describes what went wrong wile registering the plugin
   * @param stack Set to true if a stack trace should be created
   */
  constructor(message: string, stack?: boolean) {
    super('PluginRegisteringFailedError', message, 500, stack);
  }
}

/**
 * An error that is returned whenever there is a plugin request that is supposed to register a route, that is already
 * registered
 * This usually indicates that two **different** plugins use the same route.
 */
export class SCPluginRouteAlreadyRegisteredErrorResponse extends SCError {
  /**
   * Create a PluginRouteAlreadyRegisteredError
   *
   * @param registeredName The name by the plugin that has already registered the route previously
   * @param registeredUrl  The URL by the plugin that has already registered the route previously
   * @param stack Set to true if a stack trace should be created
   */
  constructor(registeredName: string, registeredUrl: string, stack?: boolean) {
    super('PluginRouteAlreadyRegisteredError',
      `Already registered by "${registeredName}" under URL "${registeredUrl}".`, 409, stack);
  }
}

/**
 * An error that is returned whenever there is a syntax error
 */
export class SCSyntaxErrorResponse extends SCError {
  /**
   * Create a SyntaxError
   *
   * @param message Describes the syntax error
   * @param stack Set to true if a stack trace should be created
   */
  constructor(message: string, stack?: boolean) {
    super('SyntaxError', message, 400, stack);
  }
}

/**
 * An error that is returned, when an internal server error occurred
 */
export class SCInternalServerErrorResponse extends SCError {
  /**
   * Internal error that occurred. If the stack is disabled this error is not set for security reasons
   */
  additionalData?: Error;

  /**
   * Create a SCInternalServerErrorResponse
   *
   * @param err Internal server error
   * @param stack Set to true if a stack trace should be created 
   * and the internal server error should be displayed to the client
   */
  constructor(err?: Error, stack?: boolean) {
    super('InternalServerError', 'Internal server error', 502, stack);

    if (stack) {
      this.additionalData = err;
    }
  }
}
