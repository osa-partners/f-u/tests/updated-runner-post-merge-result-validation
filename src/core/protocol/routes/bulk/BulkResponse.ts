/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCUuid} from '../../../types/UUID';
import {SCBulkParameters} from './BulkRequest';

/**
 * Requested Bulk from backend
 *
 * @validatable
 */
export interface SCBulkResponse extends SCBulkParameters {
  /**
   * State of bulk
   *
   * The state is `in progress` while it accepts things to be added to the bulk.
   * The state is `done` once it is closed.
   */
  state: 'in progress' | 'done';

  /**
   * Universally unique identifier of the bulk
   */
  uid: SCUuid;
}
