/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {
  SCCreativeWork,
  SCCreativeWorkMeta,
  SCCreativeWorkTranslatableProperties,
  SCCreativeWorkWithoutReferences,
} from '../base/CreativeWork';
import {SCThingWithCategoriesTranslatableProperties} from '../base/ThingWithCategories';
import {SCThingMeta, SCThingType} from '../Thing';
import {SCMetaTranslations, SCTranslations} from '../types/i18n';
import {SCPersonWithoutReferences} from './Person';

/**
 * A book without references
 */
export interface SCBookWithoutReferences extends SCCreativeWorkWithoutReferences {
  /**
   * Edition of a book
   */
  bookEdition?: string;

  /**
   * ISBN of a book
   */
  isbn: string;

  /**
   * Number of pages of a book
   */
  numberOfPages?: number;

  /**
   * Translated properties of a book
   */
  translations?: SCTranslations<SCBookTranslatableFields>;

  /**
   * Type of a book
   */
  type: SCThingType.Book;
}

/**
 * A book
 *
 * @validatable
 */
export interface SCBook extends SCCreativeWork, SCBookWithoutReferences {
  /**
   * Authors of the creative work
   */
  authors: SCPersonWithoutReferences[];

  /**
   * Translated properties of a book
   */
  translations?: SCTranslations<SCBookTranslatableFields>;

  /**
   * Type of a book
   */
  type: SCThingType.Book;
}

/**
 * Translatable properties of a book
 */
export interface SCBookTranslatableFields
  extends SCThingWithCategoriesTranslatableProperties, SCCreativeWorkTranslatableProperties {
  /**
   * Translation of an edition of a book
   */
  bookEdition?: string;
}

/**
 * Meta information about a book
 */
export class SCBookMeta extends SCThingMeta implements SCMetaTranslations<SCBook> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ... SCCreativeWorkMeta.getInstance().fieldTranslations.de,
    },
    en: {
      ... SCCreativeWorkMeta.getInstance().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCCreativeWorkMeta.getInstance().fieldValueTranslations.de,
      type: 'Buch',
    },
    en: {
      ... SCCreativeWorkMeta.getInstance().fieldValueTranslations.en,
      type: SCThingType.Book,
    },
  };
}
