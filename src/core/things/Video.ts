/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCCreativeWork, SCCreativeWorkMeta, SCCreativeWorkWithoutReferences} from '../base/CreativeWork';
import {SCThingMeta, SCThingType} from '../Thing';
import {SCLanguage, SCMetaTranslations} from '../types/i18n';
import {SCISO8601Duration} from '../types/Time';
import {SCPersonWithoutReferences} from './Person';

/**
 * A video without references
 */
export interface SCVideoWithoutReferences extends SCCreativeWorkWithoutReferences {
  /**
   * The Duration of the Video
   */
  duration?: SCISO8601Duration;

  /**
   * Downloadable/Streamable Files of the Video
   */
  sources?: SCVideoSource[];

  /**
   * URLs to a thumbnails for the Video
   */
  thumbnails?: string[];

  /**
   * Track Files for the Video
   */
  tracks?: SCVideoTrack;

  /**
   * A Transcript of the Video
   */
  transcript?: string;

  /**
   * Type of an Video
   */
  type: SCThingType.Video;
}

export interface SCVideoSource {
  /**
   * Pixel Height of the Video
   */
  height?: number;

  /**
   * MIME-Type of the source File
   */
  mimeType: SCVideoMimeType;

  /**
   * Size of the Video File in bytes
   */
  size?: number;

  /**
   * URL to the Video File
   */
  url: string;

  /**
   * Pixel Width of the Video
   */
  width?: number;
}

export interface SCVideoTrack {
  /**
   * Language of the Subtitle
   */
  language: SCLanguage;

  /**
   * Content Type of the Track File
   */
  type: SCVideoTrackTypes;

  /**
   * URL to the Track File
   */
  url: string;
}

/**
 * A video
 *
 * @validatable
 */
export interface SCVideo extends SCCreativeWork, SCVideoWithoutReferences {
  /**
   * Persons acting in the Video
   */
  actors?: SCPersonWithoutReferences[];

  /**
   * Type of a video
   */
  type: SCThingType.Video;
}

/**
 * Meta information about a video
 */
export class SCVideoMeta extends SCThingMeta implements SCMetaTranslations<SCVideo> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ... SCCreativeWorkMeta.getInstance().fieldTranslations.de,
    },
    en: {
      ... SCCreativeWorkMeta.getInstance().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCCreativeWorkMeta.getInstance().fieldValueTranslations.de,
      type: 'Video',
    },
    en: {
      ... SCCreativeWorkMeta.getInstance().fieldValueTranslations.en,
      type: SCThingType.Video,
    },
  };
}

/**
 * Video Encoding Formats
 */
export type SCVideoMimeType =
  'video/mp4'
  | 'video/ogg'
  | 'video/webm'
  | 'application/vnd.apple.mpegurl'
  | 'application/dash+xml';

/**
 * Video Track Types
 */
export type SCVideoTrackTypes =
  'captions'
  | 'chapters'
  | 'description'
  | 'metadata'
  | 'subtitles';
