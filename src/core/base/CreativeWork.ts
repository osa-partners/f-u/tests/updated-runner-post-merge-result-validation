/*
 * Copyright (C) 2018 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThing, SCThingMeta, SCThingTranslatableProperties} from '../Thing';
import {SCOrganizationWithoutReferences} from '../things/Organization';
import {SCPersonWithoutReferences} from '../things/Person';
import {SCLanguage, SCMetaTranslations, SCTranslations} from '../types/i18n';
import {SCISO8601Date} from '../types/Time';
import {
  SCAcademicPriceGroup,
  SCThingThatCanBeOffered,
  SCThingThatCanBeOfferedTranslatableProperties,
} from './ThingThatCanBeOffered';

/**
 * A creative work without references
 */
export interface SCCreativeWorkWithoutReferences extends SCThing {
  /**
   * Date the creative work was published
   */
  datePublished?: SCISO8601Date;

  /**
   * List of languages this creative work is written/recorded/... in
   */
  inLanguages?: SCLanguage[];

  /**
   * Keywords of the creative work
   */
  keywords?: string[];

  /**
   * Translated fields of the creative work
   */
  translations?: SCTranslations<SCCreativeWorkTranslatableProperties>;
}

/**
 * A creative work
 */
export interface SCCreativeWork extends SCCreativeWorkWithoutReferences, SCThingThatCanBeOffered<SCAcademicPriceGroup> {
  /**
   * Authors of the creative work
   */
  authors?: SCPersonWithoutReferences[];

  /**
   * List of publishers of the creative work
   */
  publishers?: Array<SCPersonWithoutReferences | SCOrganizationWithoutReferences>;

  /**
   * Translated fields of the creative work
   */
  translations?: SCTranslations<SCCreativeWorkTranslatableProperties>;
}

/**
 * Translatable properties of creative works
 */
export interface SCCreativeWorkTranslatableProperties
  extends SCThingTranslatableProperties, SCThingThatCanBeOfferedTranslatableProperties {
  /**
   * Translation of the keywords of the creative work
   */
  keywords?: string[];
}

/**
 * Meta information about creative works
 */
export class SCCreativeWorkMeta extends SCThingMeta implements SCMetaTranslations<SCCreativeWork> {
  /**
   * Translations of fields
   */
  fieldTranslations = {
    de: {
      ... SCThingMeta.getInstance().fieldTranslations.de,
    },
    en: {
      ... SCThingMeta.getInstance().fieldTranslations.en,
    },
  };

  /**
   * Translations of values of fields
   */
  fieldValueTranslations  = {
    de: {
      ... SCThingMeta.getInstance().fieldValueTranslations.de,
    },
    en: {
      ... SCThingMeta.getInstance().fieldValueTranslations.en,
    },
  };
}
