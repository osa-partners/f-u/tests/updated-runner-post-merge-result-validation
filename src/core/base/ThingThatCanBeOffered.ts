/*
 * Copyright (C) 2018, 2019 StApps
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 */
import {SCThing, SCThingTranslatableProperties} from '../Thing';
import {SCOrganizationWithoutReferences} from '../things/Organization';
import {SCPersonWithoutReferences} from '../things/Person';
import {SCTranslations} from '../types/i18n';
import {SCInPlace} from '../types/Places';
import {SCISO8601Date} from '../types/Time';

/**
 * Default price without distinction
 */
export interface SCPriceGroup {
  /**
   * Default price of the thing
   */
  default: number;
}

/**
 * Price distinctions for academic context
 */
export interface SCAcademicPriceGroup extends SCPriceGroup {
  /**
   * Price for employees
   */
  employee?: number;

  /**
   * Price for guests
   */
  guest?: number;

  /**
   * Price for students
   */
  student?: number;
}

/**
 * A thing without references that can be offered
 */
export interface SCThingThatCanBeOfferedWithoutReferences extends SCThing {
  /**
   * Translations of a thing that can be offered
   */
  translations?: SCTranslations<SCThingThatCanBeOfferedTranslatableProperties>;
}

/**
 * A thing that can be offered
 */
export interface SCThingThatCanBeOffered<T extends SCPriceGroup>
  extends SCThing {
  /**
   * List of offers for that thing
   */
  offers?: Array<SCThingThatCanBeOfferedOffer<T>>;
}

/**
 * Offer of a thing
 */
export interface SCThingThatCanBeOfferedOffer<T extends SCPriceGroup>
  extends SCInPlace {
  /**
   * Availability of an offer
   */
  availability: SCThingThatCanBeOfferedAvailability;

  /**
   * The time when the thing becomes unavailable as an SCISO8601Date formatted string.
   */
  availabilityEnds?: SCISO8601Date;

  /**
   * The time when the thing becomes available as an SCISO8601Date formatted string.
   */
  availabilityStarts?: SCISO8601Date;

  /**
   * List of prices that are distinct for specific groups
   */
  prices: T;

  /**
   * Provider of an offer
   */
  provider: SCThingThatCanBeOfferedProvider;
}

/**
 * Translatable properties of a thing that can be offered
 */
export interface SCThingThatCanBeOfferedTranslatableProperties
  extends SCThingTranslatableProperties {
  /**
   * Availability of an offer
   */
  'offers[].availability'?: string;
}

/**
 * Entity responsible for the offer
 */
export type SCThingThatCanBeOfferedProvider =
  | SCOrganizationWithoutReferences
  | SCPersonWithoutReferences;

/**
 * Availability of an Offer
 */
export type SCThingThatCanBeOfferedAvailability =
  | 'in stock'
  | 'out of stock'
  | 'online only'
  | 'limited availability';
